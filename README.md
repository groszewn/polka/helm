# helm

Install and manage Helm

## Dependencies

* [polka.asdf](https://gitlab.com/discr33t/polka/asdf.git)
  _The asdf configs must be included in the users `playbook.yml` since no
  default configs are passed to the dependent role_

## Role Variables

* `versions`:
    * Type: List
    * Usages: List of versions of Helm to install

* `global_version`:
    * Type: String
    * Usages: Helm version to make global default

```
helm:
  versions:
    - 1.8.3
    - 2.9.2
  global_version: 2.9.2
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polka.helm

## License

MIT
